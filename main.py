'''
 * Name: Demetri Van Sickle
 * Date: 06/07/2024
 * Class: CS302
 * Assignment: Program 4/5
 * Description:
 *      Main file for the game that runs the gameRunner class
'''
import gameRunner

if __name__ == "__main__":
    gr = gameRunner.GameRunner()

    gr.setup()
    gr.run()