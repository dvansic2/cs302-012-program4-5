'''
 * Name: Demetri Van Sickle
 * Date: 06/04/2024
 * Class: CS302
 * Assignment: Program 4/5
 * Description:
 *      Contains the gameRunner class
'''
import BST
import actions
import random
import os

class GameRunner: #TODO: add fucntion comments
    def __init__(self):
        self.__sceneList = []
        self.__playerName = ""
        self.__bst = BST.BST()
        self.__isSetup = False
        self.__filler = [
            "As the spaceship glides through the vast expanse of space, [NAME] takes a moment to review the mission logs. The stars outside the viewport twinkle like distant beacons, each one holding the promise of new discoveries. It's a perfect time to prepare for the next challenge.",
            "[NAME] performs a routine systems check on the spaceship. The hum of the engines and the steady blip of the radar provide a comforting background noise. Everything seems to be in order, but in space, it's always best to stay vigilant. Ready for the next adventure, [NAME] sets the course.",
            "Taking a break from the constant stream of tasks, [NAME] gazes out at a nearby nebula, its swirling colors a reminder of the beauty and mystery of the universe. Reflecting on the journey so far, [NAME] feels a renewed sense of purpose. The next mission awaits, full of unknown challenges and opportunities.",
            "[NAME] sits back in the pilot's chair, watching the endless stretch of space outside the cockpit window. The quiet hum of the spaceship's life support systems and the distant glimmers of far-off galaxies create a serene atmosphere. It's a brief moment of calm before the next mission briefing lights up the console, signaling new adventures on the horizon.",
        ]
        self.__intro = "You are [NAME], a brave and talented space cadet, chosen for a critical mission to explore uncharted planets and gather vital data to aid humanity's expansion into the cosmos. However, disaster strikes. A sudden meteor storm damages your spaceship, knocking your AI assistant, ALPHA, offline. Stranded in the vast expanse of space, you must rely on your programming skills to navigate through random encounters and limp back to the nearest base."
        self.__outro = "After a series of harrowing encounters and daring escapes, [NAME] finally makes it back to base. The mission was a success, despite the setbacks and challenges along the way. ALPHA is back online, and the data you've collected will help humanity take its next steps into the unknown. As you step out of the spaceship, you can't help but feel a sense of pride and accomplishment. The stars are waiting, and the universe is full of possibilities. Until next time, space cadet."

    '''
     * Description:
     *      Sets up the game by getting the player's name and creating a list of scenes
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *     None
    '''
    def setup(self):
        valid = False

        # Get the player's name
        while not valid:
            try:
                self.__getName()
                valid = True
            except ValueError as e:
                print(str(e) + "\n")

        valid = False
        while not valid:
            # Create the scenes list
            for i in range(10): 
                scene = self.__pickRandScene()
                self.__sceneList.append(scene)

            # Make sure the list is valid
            if self.__listValid():
                valid = True
            else:
                self.__sceneList = []
                    
        self.__isSetup = True

    '''
     * Description:
     *      Runs the game
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *     None 
    '''
    def run(self):
        valid = False

        # Display the intro
        self.__clearScreen()
        print(self.__intro.replace("[NAME]", self.__playerName))
        input("\nPress Enter to continue...")

        # Dont run if setup has not been called
        if not self.__isSetup:
            raise ValueError("Setup must be called before run")

        # Run the scenes
        for scene in self.__sceneList:
            
            if scene == None:
                self.__clearScreen()
                print(self.__getFiller())
                input("\nPress Enter to continue...")
            else:
                # Perform the scene's action
                valid = False
                while not valid:
                    try:
                        self.__clearScreen()
                        scene.performAction()
                        input("\nPress Enter to continue...")
                        valid = True
                    except ValueError:
                        print("Invalid input. Please try again.\n")
            
            # Save the scenes
            if scene != None:
                self.__bst.insert(scene)

        # Display the outro
        self.__clearScreen()
        print(self.__outro.replace("[NAME]", self.__playerName))

        # Display the total score
        print("\n\nTotal score: " + str(self.__bst.getTotal()))

    '''
     * Description:
     *      Randomly selects a scene type
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *      returns a scene object, or None 
    '''
    def __pickRandScene(self):
        randNum = random.randint(1, 4)

        if randNum == 1:
            return actions.Statement(self.__playerName)
        elif randNum == 2:  
            return actions.Decision(self.__playerName)
        elif randNum == 3:
            return actions.Loop(self.__playerName)
        else:
            return None

    '''
     * Description:
     *      Gets the player's name, raises an exception if the name is empty
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *      None
    '''
    def __getName(self):
        self.__playerName = input("What is your name? ")

        if self.__playerName == "":
            raise ValueError("Name cannot be empty")
    
    '''
     * Description:
     *      Checks if the list of scenes is valid
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *     true - if the list is valid
     *     false - if the list is not 
    '''
    def __listValid(self):
        numStatements = 0
        numDecisions = 0
        numLoops = 0
        numEmpty = 0
        prevScene = None
        sequenceError = False

        # Cycle through the list
        for scene in self.__sceneList:
            
            # Make sure the same sequence of scenes doesn't happen twice in a row
            if type(prevScene) == type(scene):
                sequenceError = True

            # Count the number of each type of scene
            if scene == None:
                numEmpty += 1
            elif type(scene) == actions.Statement:
                numStatements += 1
            elif type(scene) == actions.Decision:
                numDecisions += 1
            elif type(scene) == actions.Loop:
                numLoops += 1

            prevScene = scene

        # Make sure there is at least one of each type of scene and no more than 5 empty scenes
        if sequenceError or numStatements < 1 or numDecisions < 1 or numLoops < 1 or numEmpty > 5:
            return False
        else:
            return True
    
    '''
     * Description:
     *      Randomly selects a filler text and replaces the [NAME] placeholder with the player's name
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *     None
    '''
    def __getFiller(self):
        return random.choice(self.__filler).replace("[NAME]", self.__playerName)


    '''
     * Description:
     *      Clears the screen
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *     None
    '''
    def __clearScreen(self):
        # For Windows
        if os.name == 'nt':
            os.system('cls')
        # For Linux
        else:
            os.system('clear')
        