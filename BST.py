'''
 * Name: Demetri Van Sickle
 * Date: 06/01/2024
 * Class: CS302
 * Assignment: Program 4/5
 * Description:
 *      Contains the BST class and node class
'''
import actions

#--------------------------------------------------------------------------------
# BEGIN Node class
#--------------------------------------------------------------------------------
class Node:
    def __init__(self, data):
        self.__data = data
        self.__right = None
        self.__left = None

    def getRight(self):
        return self.__right
    
    def getLeft(self):
        return self.__left
    
    def getData(self):
        return self.__data
    
    def setRight(self, newRight):
        self.__right = newRight

    def setLeft(self, newLeft):
        self.__left = newLeft

    def setData(self, newData):
        self.__data = newData
#--------------------------------------------------------------------------------
# END Node class
#--------------------------------------------------------------------------------


#--------------------------------------------------------------------------------
# BEGIN BST
#--------------------------------------------------------------------------------
class BST:
    def __init__(self):
        self.__root = None
        self.__size = 0

    '''
     * Description:
     *      Inserts a new item into the BST, duplicates are allowed
     * 
     * Arguments:
     *     data - the data to insert
     * 
     * Returns:
     *      None
    '''
    def insert(self, data):  
        if self.__root == None:
            newNode = Node(data)
            self.__root = newNode
            self.__size += 1
            return
        else:
            self.__insertR(self.__root, self.__root, data)

    '''
     * Description:
     *      Retrieves the size of the BST
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *      int - the size of the BST
    '''
    def getSize(self):
        return self.__size

    '''
     * Description:
     *      Retrieves the first instance of the data in the BST
     * 
     * Arguments:
     *     data - the data to retrieve
     * 
     * Returns:
     *      Node - the node containing the data
    '''
    def retrieve(self, data):
        return self.__retrieveR(self.__root, data)

    '''
     * Description:
     *      Removes all instances of the data in the BST
     * 
     * Arguments:
     *     data - the data to remove
     * 
     * Returns:
     *      None
    '''
    def remove(self, data):
        self.__removeR(self.__root, self.__root, data)

    '''
     * Description:
     *      Displays the BST in post order
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *      None
    '''
    def display(self):
        if self.__root == None:
            print("\nTree is empty")
        else:
            self.__displayR(self.__root)
    
    '''
     * Description:
     *      Returns the score total
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *      int - score total
    '''
    def getTotal(self):
        return self.__getTotalR(self.__root)

    '''
     * Description:
     *      Recursive function to return the score total
     * 
     * Arguments:
     *     currentNode - the current node
     * 
     * Returns:
     *     int - score total
    '''
    def __getTotalR(self, currentNode):
        total = 0
        
        # Base case
        if currentNode == None:
            return total
        
        # Add it to the total
        if isinstance(currentNode.getData(), actions.Actions):
            total += currentNode.getData().getScore()
        else:
            total += currentNode.getData()

        total += self.__getTotalR(currentNode.getLeft())

        return total + self.__getTotalR(currentNode.getRight())

    '''
     * Description:
     *      Recursive function to display the BST in post order
     * 
     * Arguments:
     *     currentNode - the current node to display
     * 
     * Returns:
     *      None
    '''
    def __displayR(self, currentNode):
        # Base case
        if currentNode == None:
            return
        
        self.__displayR(currentNode.getLeft())
        self.__displayR(currentNode.getRight())
        print("=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*")
        if isinstance(currentNode.getData(), actions.Actions):
            print(currentNode.getData().display())
        else:
            print(currentNode.getData())
        print("=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*")

    '''
     * Description:
     *      Recursive function to remove the data from the BST
     * 
     * Arguments:
     *     currentNode - the current node to check
     *     prevNode - the previous node
     *     data - the data to remove
     * 
     * Returns:
     *      None
    '''
    def __removeR(self, currentNode, prevNode, data):
        # Base case
        if currentNode == None:
            return None
        
        # If we match 
        if currentNode.getData() == data:
            #-------------------------------------------------------
            # Case: we have BOTH a left and right child
            #-------------------------------------------------------
            if currentNode.getLeft() != None and currentNode.getRight() != None:
                # Get IOS
                iosNode = self.__returnIosR(currentNode.getRight(), currentNode)

                #Re assign the data
                currentNode.setData(iosNode.getData())

                # Check this node again, as the ios might also be the one we want to remove
                self.__removeR(currentNode, prevNode, data)

                self.__size -= 1

            #-------------------------------------------------------
            # Case: we have ONLY a left child
            #-------------------------------------------------------
            elif currentNode.getLeft() != None:
                # Link our parent to our child
                if prevNode.getLeft() == currentNode:
                    prevNode.setLeft(currentNode.getLeft())
                else:
                    prevNode.setRight(currentNode.getLeft())

                self.__size -= 1

            #-------------------------------------------------------
            # Case: we have ONLY a right child
            #-------------------------------------------------------
            elif currentNode.getRight() != None:
                # Link our parent to our child
                if prevNode.getLeft() == currentNode:
                    prevNode.setLeft(currentNode.getRight())
                else:
                    prevNode.setRight(currentNode.getRight())

                self.__size -= 1

            #-------------------------------------------------------
            # Case: we have NO children
            #-------------------------------------------------------
            elif currentNode.getLeft() == None and currentNode.getRight() == None:
                # Sever our parents link to us
                if prevNode.getLeft() == currentNode:
                    prevNode.setLeft(None)
                else:
                    prevNode.setRight(None)

                self.__size -= 1

        #-------------------------------------------------------
        # If less than
        #-------------------------------------------------------
        if data < currentNode.getData():
            self.__removeR(currentNode.getLeft(), currentNode, data)

        #-------------------------------------------------------
        # If greater than or equal
        #-------------------------------------------------------
        elif data >= currentNode.getData():
            self.__removeR(currentNode.getRight(), currentNode, data)

    '''
     * Description:
     *      Recursive function to return the inorder successor
     * 
     * Arguments:
     *     currentNode - the current node to check
     *     prevNode - the previous node
     * 
     * Returns:
     *     Node - the inorder successor
    '''
    def __returnIosR(self, currentNode, prevNode):
        # Base case
        if currentNode.getLeft() == None:

            # If we have a right child, take care of it
            if currentNode.getRight() != None:
                # Link our parent to our child
                if prevNode.getLeft() == currentNode:
                    prevNode.setLeft(currentNode.getRight())
                else:
                    prevNode.setRight(currentNode.getRight())

                # Reset our right child pointer
                currentNode.setRight(None)
            else:
                if prevNode.getLeft() == currentNode:
                    prevNode.setLeft(None)
                else:
                    prevNode.setRight(None)

            return currentNode
        
        return self.__returnIosR(currentNode.getLeft(), currentNode)

    '''
     * Description:
     *      Recursive function to retrieve the data from the BST
     * 
     * Arguments:
     *     currentNode - the current node to check
     *     data - the data to retrieve
     * 
     * Returns:
     *      Node - the node containing the data
    '''
    def __retrieveR(self, currentNode, data):
        # Base case
        if currentNode == None:
            return None
        
        #-------------------------------------------------------
        # If equal, return
        #-------------------------------------------------------
        if currentNode.getData() == data:
            return currentNode
        
        #-------------------------------------------------------
        # If less than
        #-------------------------------------------------------
        if data < currentNode.getData():
            return self.__retrieveR(currentNode.getLeft(), data)

        #-------------------------------------------------------
        # If greater than or equal
        #-------------------------------------------------------
        elif data >= currentNode.getData():
            return self.__retrieveR(currentNode.getRight(), data)

    '''
     * Description:
     *      Recursive function to insert the data into the BST
     * 
     * Arguments:
     *     currentNode - the current node to check
     *     prevNode - the previous node
     *     data - the data to insert
     * 
     * Returns:
     *      None
    '''
    def __insertR(self, currentNode, prevNode, data):
        # Base case
        if currentNode == None:

            newNode = Node(data)

            if data < prevNode.getData():
                prevNode.setLeft(newNode)
            elif data >= prevNode.getData():
                prevNode.setRight(newNode)

            self.__size += 1

            return
        
        #-------------------------------------------------------
        # If less than
        #-------------------------------------------------------
        if data < currentNode.getData():
            self.__insertR(currentNode.getLeft(), currentNode, data)

        #-------------------------------------------------------
        # If greater than or equal
        #-------------------------------------------------------
        elif data >= currentNode.getData():
            self.__insertR(currentNode.getRight(), currentNode, data)

    '''
     * Description:
     *     Traverse the BST in order and return the list 
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *      list - the list items in the BST in order
    '''
    def __inorderTraversal(self):
        result = []
        
        self.__inorderTraversalR(self.__root, result)
        
        return result

    '''
     * Description:
     *      Recursive function to traverse the BST in order
     * 
     * Arguments:
     *     currentNode - the current node to check
     *     result - the list to append to
     * 
     * Returns:
     *      None
    '''
    def __inorderTraversalR(self, currentNode, result):
        # Base Case
        if currentNode == None:
            return
        
        self.__inorderTraversalR(currentNode.getLeft(), result)
        result.append(currentNode.getData())
        self.__inorderTraversalR(currentNode.getRight(), result)
