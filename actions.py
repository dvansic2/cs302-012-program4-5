'''
 * Name: Demetri Van Sickle
 * Date: 05/31/2024
 * Class: CS302
 * Assignment: Program 4/5
 * Description:
 *      Contains the Actions class hirearchy and its methods
'''
import random
import re
import numpy as np
import os

#--------------------------------------------------------------------------------
# BEGIN Actions class
#--------------------------------------------------------------------------------
class Actions:
    def __init__(self, name):
        self._score = 0
        self._correctIndex = 0
        self._failMessages = None
        self._successMessages = None
        self._descriptions = None
        self._userInput = None

        if isinstance(name, str) and len(name) > 0:
            self._name = name
        else:
            raise ValueError("Invalid name")
        
    def __lt__(self, other):
        if isinstance(other, Actions):
            return self._score < other._score
        if isinstance(other, int):
            return self._score < other
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, Actions):
            return self._score > other._score
        if isinstance(other, int):
            return self._score > other
        else:
            return NotImplemented
    
    def __ge__(self, other):
        if isinstance(other, Actions):
            return self._score >= other._score
        if isinstance(other, int):
            return self._score >= other
        else:
            return NotImplemented
        
    def __le__(self, other):
        if isinstance(other, Actions):
            return self._score <= other._score
        if isinstance(other, int):
            return self._score <= other
        else:
            return NotImplemented
        
    def __eq__(self, other):
        if isinstance(other, Actions):
            return self._score == other._score
        if isinstance(other, int):
            return self._score == other
        else:
            return NotImplemented
        
    '''
     * Description:
     *      Clears the screen
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *     None
    '''
    def _clearScreen(self):
        # For Windows
        if os.name == 'nt':
            os.system('cls')
        # For Linux
        else:
            os.system('clear')

    '''
     * Description:
     *      Returns the score of the action
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *     int - the score of the action
    '''
    def getScore(self):
        return self._score

#--------------------------------------------------------------------------------
# END Actions class
#--------------------------------------------------------------------------------

#--------------------------------------------------------------------------------
# BEGIN Statement class
#--------------------------------------------------------------------------------

class Statement(Actions):
    def __init__(self, name):
        
        # Call the parent constructor
        super().__init__(name)

        self.__commands = np.array([
            "move left",
            "move right",
            "speed up",
            "slow down",
        ])

        self._successMessages = [
            "You deftly maneuver the ship to the left, narrowly avoiding the rock. The ship's sensors show a clear path ahead for now.", 
            "With a quick flick of the controls, you shift the ship to the right. The rock zooms past harmlessly, and you breathe a sigh of relief.",
            "You punch the thrusters, and the ship surges forward. The rocks fall behind, unable to keep up with the ship's increased speed. You look ahead, scanning for the next challenge.",
            "You reduce the ship's speed, allowing for more precise maneuvering. The ship weaves through the debris field at a safer pace, avoiding the massive rock and other smaller obstacles with ease.",   
        ]

        self._failMessages = [
            "Wrong command! The ship's hull is damaged from scraping the rock. Try to execute the correct command next time!",
            "Wrong command! The rock clipped the ship, sending it into a spin. Select the correct command to avoid collisions. Eventually the auto thrusters being the ship steady again",
            "Wrong command! The rocks have hit the engines, causing them to malfunction. Speed up sooner to stay ahead of danger! Eventually the nanobots are able to repair the engines and you are on your way again.",
            "Wrong command! The ship collides with the rock head-on, causing severe damage. Slow down to navigate safely through debris fields. Eventually the nanobots are able to repair the hull to a passable state as the ship continues to limp on.",
        ]

        self._descriptions = [
            "As you pilot the ship through an asteroid belt, a large, jagged rock suddenly appears on the right side of your ship. The ship's proximity alarm blares, warning of an imminent collision.",
            "Your ship glides through a serene patch of space when the sensors detect a massive, spinning rock hurtling from the left. The rock's trajectory is erratic, posing a significant threat.",
            "Cruising along at a steady pace, you notice a cluster of smaller rocks converging behind the ship. The rocks are gaining speed, and it's only a matter of time before they catch up.",
            "As you approach a dense field of floating debris, the ship's radar picks up a massive rock directly ahead, moving unpredictably. At the current speed, navigating through without a collision seems impossible.",
        ]

        self._correctIndex = random.randint(0, len(self.__commands) - 1)

    '''
     * Description:
     *      performs the action of the statement
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *      None
    '''
    def performAction(self):
        print(self._descriptions[self._correctIndex])

        # Print out the commands the player can use
        print("You have the following commands: ")
        for command in self.__commands:
            print(" " + command)

        # Get the user input
        self._userInput = input("\nEnter your command: ")

        # Validate the command
        if not self.__validateCommand():
            raise ValueError("Invalid command")
        
        # Execute the command
        if self.__executeCommand():
            self._score = random.randint(1, 4)
        else:
            self._score = random.randint(-4, -1)

    
    '''
     * Description:
     *      Validates the command from the user
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *      false - if the command is invalid
     *      true - if the command is valid
    '''
    def __validateCommand(self):
        if isinstance(self._userInput, str) and self._userInput.lower() in self.__commands:
            return True
        else:
            return False
        
    '''
     * Description:
     *      Executes the command from the user
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *      true - if the command is executed
     *      false - if the command is not executed
    '''
    def __executeCommand(self):
        if self._userInput.lower() == self.__commands[self._correctIndex]:
            print(self._successMessages[self._correctIndex])
            return True
        else:
            print(self._failMessages[self._correctIndex])
            return False
    
    '''
     * Description:
     *      Prints out the correct command, the command the user entered, and the score for this scenario
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *      None
    '''
    def display(self):
        print("Correct command: " + self.__commands[self._correctIndex])
        print("Player command: " + self._userInput)
        print("Score: " + str(self._score))

#--------------------------------------------------------------------------------
# END Statement class
#--------------------------------------------------------------------------------


#--------------------------------------------------------------------------------
# BEGIN Decision class
#--------------------------------------------------------------------------------
class Decision(Actions):
    def __init__(self, name):
        
        # Call the parent constructor
        super().__init__(name)

        self.__actions = [
            "fire at ship",
            "hail ship"
        ]

        self.__evaluations = [
            "ship is hostile",
            "ship is friendly"
        ]

        self.__parsedUserStatement = None

        self._successMessages = [
            "You swiftly initiate the ship's defensive systems and fire at the hostile vessel. The enemy ship takes critical damage and retreats.", 
            "You open a communication channel and hail the friendly ship. The crew of the other vessel responds warmly, offering valuable information and supplies."
        ]

        self._failMessages = [
            "The logic you entered failed! The ship ends up being hostile and manages to get too close and fires at your ship, causing significant damage. The bots repair the ship, but not after some delay.",
            "The logic you entered failed! You fired at a friendly ship, causing unnecessary damage, and provoking a defensive response. The mistake strains potential alliances."
        ]

        self._descriptions = [
            "Your ship’s radar displays an approaching ship. Sensors indicate that the approaching ship has the possibility of being hostile, construct an if statement to decide the proper action.",
            "Your ship’s radar displays an approaching ship. Sensors indicate that the approaching ship looks to be unarmed, construct an if statement to decide the proper action."
        ]

        self._correctIndex = random.randint(0, len(self.__actions) - 1)


    '''
     * Description:
     *      Asks player for choice and runs it
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *     None
    '''
    def performAction(self):
        # Print the description to set the scene
        print(self._descriptions[self._correctIndex])

        # Print the instructions
        print("\nWrite an \"if\" statement that follows the following format (keep the [])")
        print("if [evaluation] then [action]")
        print("Evaluations:\n   [ship is hostile]\n   [ship is friendly]")
        print("\nActions:\n   [fire at ship]\n   [hail ship]\n\n")

        self._userInput = input("Your statement: ")

        # Validate
        if not self.__validateAndParse():
            raise ValueError
        
        # Check if the answer is correct
        if self.__testStatement():
            self._score = random.randint(1, 4)
        else:
           self._score = random.randint(-4, -1) 


    '''
     * Description:
     *      Parses and validates user input
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *      true - input parsed and valid
     *      false -  input was unable to be parsed and/or is invalid
    '''
    def __validateAndParse(self):

        # Parse the input if its valid
        if self._userInput != "" and isinstance(self._userInput, str):
            self.__parsedUserStatement = re.findall(r'\[(.*?)\]', self._userInput)
        else:
            return False 
        
        # Make sure the parse was correct
        if len(self.__parsedUserStatement) != 2:
            return False
        
        # Set the parsed statements to lowercase
        self.__parsedUserStatement = [s.lower() for s in self.__parsedUserStatement]

        # Make sure the evaluation is valid
        if self.__parsedUserStatement[0] not in self.__evaluations:
            return False

        # Make sure the action is valid
        if self.__parsedUserStatement[1] not in self.__actions:
            return False
        
        # If we get this far, we are all good
        return True
    

    '''
     * Description:
     *      Tests if the player choices were correct
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *      true - choices were correct
     *      false - choices were not corrrect
    '''
    def __testStatement(self):
        index = self._correctIndex

        if self.__parsedUserStatement[0] == self.__evaluations[index] and self.__parsedUserStatement[1] == self.__actions[index]:
            print(self._successMessages[index])
            return True
        else:
            print(self._failMessages[index])
            return False
        
    '''
     * Description:
     *      Prints out the correct statement, the statement the user entered, and the score for this scenario
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *      None
    '''
    def display(self):
        print("Correct statement: if [" + self.__evaluations[self._correctIndex] + "] then [" + self.__actions[self._correctIndex] + "]")
        print("Player statement: " + self._userInput)
        print("Score: " + str(self._score))
        
#--------------------------------------------------------------------------------
# END Decision class
#--------------------------------------------------------------------------------


#--------------------------------------------------------------------------------
# BEGIN Loop class
#--------------------------------------------------------------------------------
class Loop(Actions):
    def __init__(self, name):
        
        # Call the parent constructor
        super().__init__(name)

        self._successMessages = [
            "You swiftly collect all the valuable rocks and continue on your way.", 
        ]

        self._failMessages = [
            "You overwhelmed the system by moving the counter in the wrong direction and never exiting the loop. The ship’s safety kicked in and aborted the collection. Better luck next time.",
        ]

        self._descriptions = [
            "The ship shows a collection of valuable asteroids ahead. The while loop in the collection system needs some help operating though. Help the system break out of the loop.",
        ]

        self._correctIndex = 0

        self.__numAsteroids = random.randint(4, 10)

        self.__numAsteroidsCollected = 0


    '''
     * Description:
     *      Runs the scenario
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *     None 
    '''
    def performAction(self):
        # Display description
        print(self._descriptions[0])

        isComplete = False

        # Loop until 
        while not isComplete:
            self.__collectSample()

            isComplete = self.__isComplete()

        # Print out the result message
        if self.__getResult():
            self._score = random.randint(1, 4)
        else:
            self._score = random.randint(-4, -1) 


    '''
     * Description:
     *      Promts player for choice and increments/decrements the asteroid count
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *     None
    '''
    def __collectSample(self):
        inputIsValid = False

        # Get choice from player
        while not inputIsValid:
            self._clearScreen()
            print(self._descriptions[0] + "\n\n")
            print("While numAsteroidsCollected[" + str(self.__numAsteroidsCollected) + "] <= numAsteroids[" + str(self.__numAsteroids) + "]")
            print("\nChoose:\n    [0] decrement numAsteroidsCollected\n    [1] increment numAsteroidsCollected")

            self._userInput = input("\n: ")

            try:
                if self._userInput not in ["0", "1"]:
                    raise ValueError
                else:
                    inputIsValid = True
            except:
                print("Invalid, press enter to continue")
                input()
            
        if self._userInput == "0":
            self.__numAsteroidsCollected -= 1
        else:
            self.__numAsteroidsCollected += 1

    
    '''
     * Description:
     *      Checks if the conditions are met to leave the loop
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *     true - numAsteroidsCollected >= numAsteroids OR < 0
     *     false - otherwise
    '''
    def __isComplete(self):
        if self.__numAsteroidsCollected >= self.__numAsteroids or self.__numAsteroidsCollected < 0:
            return True
        else:
            return False
        

    '''
     * Description:
     *      Deterimines what success message to print
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      true - successfully collected
     *      false - not successfully collected
    '''
    def __getResult(self):
        if self.__numAsteroidsCollected >= self.__numAsteroids:
            print(self._successMessages[0])
            return True
        else:
            print(self._failMessages[0])
            return False
        

    '''
     * Description:
     *      Prints out the number of asteroids collected, the number of asteroids to collect, and the score for this scenario
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *      None
    '''
    def display(self):
        print("Asteroids collected: " + str(self.__numAsteroidsCollected))
        print("Asteroids to collect: " + str(self.__numAsteroids))
        print("Score: " + str(self._score))

#--------------------------------------------------------------------------------
# END Loop class
#--------------------------------------------------------------------------------