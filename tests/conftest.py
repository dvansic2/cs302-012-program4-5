'''
 * Name: Demetri Van Sickle
 * Date: 05/31/2024
 * Class: CS302
 * Assignment: Program 4/5
 * Description:
 *      Pytest fixtures for actions.py
'''
import pytest
import actions
import gameRunner
import random
from unittest.mock import patch

#-------------------------------------------------------
# Action class fixtures
#-------------------------------------------------------
@pytest.fixture
def actionsObj():
    return actions.Actions("test")

@pytest.fixture
def statementObj():
    return actions.Statement("test")

@pytest.fixture
def decisionObj():
    return actions.Decision("test")

@pytest.fixture
def loopObj():
    return actions.Loop("test")

#-------------------------------------------------------
# Mocked fucntions
#-------------------------------------------------------
@pytest.fixture
def mock_input(request):
    with patch('builtins.input', return_value=request.param) as mocked_input:
        yield mocked_input

@pytest.fixture
def mock_input_str():
    with patch('builtins.input', return_value="test") as mocked_input_str:
        yield mocked_input_str

@pytest.fixture
def mock_statement_validateCommand(request):
    with patch('actions.Statement._Statement__validateCommand', return_value=request.param) as mocked_statement_validateCommand:
        yield mocked_statement_validateCommand

@pytest.fixture
def mock_decision_validateAndParse(request):
    with patch('actions.Decision._Decision__validateAndParse', return_value=request.param) as mocked_decision_validateAndParse:
        yield mocked_decision_validateAndParse

@pytest.fixture
def mock_decision_testStatement(request):
    with patch('actions.Decision._Decision__testStatement', return_value=request.param) as mocked_decision_testStatement:
        yield mocked_decision_testStatement

@pytest.fixture
def mock_decision_performAction(request = True):
    with patch('actions.Decision.performAction', return_value=request) as mocked_decision_performAction:
        yield mocked_decision_performAction

@pytest.fixture
def mock_random_randint(request):
    with patch('random.randint', return_value=request.param) as mocked_random_randint:
        yield mocked_random_randint

@pytest.fixture
def mock_gameRunner_pickRandScene(request = None):
    with patch('gameRunner.GameRunner._GameRunner__pickRandScene', return_value=request) as mocked_gameRunner_pickRandScene:
        yield mocked_gameRunner_pickRandScene

@pytest.fixture
def mock_gameRunner_getName(request = None):
    with patch('gameRunner.GameRunner._GameRunner__getName', return_value=request) as mocked_gameRunner_getName:
        yield mocked_gameRunner_getName

@pytest.fixture
def mock_gameRunner_listValid(request = True):
    with patch('gameRunner.GameRunner._GameRunner__listValid', return_value=request) as mocked_gameRunner_listValid:
        yield mocked_gameRunner_listValid