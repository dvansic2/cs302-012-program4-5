'''
 * Name: Demetri Van Sickle
 * Date: 06/12/2024
 * Class: CS302
 * Assignment: Program 4/5
 * Description:
 *      Holds the test suite for gameRunner.py
'''
import pytest
import gameRunner
import actions

#-------------------------------------------------------
# Constructor
#-------------------------------------------------------
def test_gameRunner_constructor():
    gr = gameRunner.GameRunner()
    assert gr._GameRunner__sceneList == []
    assert gr._GameRunner__playerName == ""
    assert gr._GameRunner__bst != None
    assert gr._GameRunner__isSetup == False
    assert len(gr._GameRunner__filler) > 0
    assert len(gr._GameRunner__intro) > 0
    assert len(gr._GameRunner__outro) > 0


#-------------------------------------------------------
# __pickRandScene
#  w/ mocked random.randint
#-------------------------------------------------------
@pytest.mark.parametrize("mock_random_randint, expected", [
    (1, "<class 'actions.Statement'>"),
    (2, "<class 'actions.Decision'>"),
    (3, "<class 'actions.Loop'>"),
    (4, "<class 'NoneType'>")
], indirect=["mock_random_randint"])
def test_gameRunner_pickRandScene(mock_random_randint, expected):
    # Setup
    gr = gameRunner.GameRunner()
    gr._GameRunner__playerName = "test"
    
    # Run
    result = gr._GameRunner__pickRandScene()

    # Verify
    assert str(type(result)) == expected


#-------------------------------------------------------
# __getName
#  w/ mocked input
#-------------------------------------------------------
@pytest.mark.parametrize("mock_input, expected", [
    ("test", "test"), # Case: valid input
    ("", None) # Case: empty input
], indirect=["mock_input"])
def test_gameRunner_getName(mock_input, expected):
    gr = gameRunner.GameRunner()

    # If expected is None (aka an empty string), we expect an exception
    if expected == None:
        with pytest.raises(ValueError):
            gr._GameRunner__getName()
    
    # Otherwise, we expect the name to be set
    else:
        gr._GameRunner__getName()
        assert gr._GameRunner__playerName == expected


#-------------------------------------------------------
# __listValid
#-------------------------------------------------------
@pytest.mark.parametrize("sceneList, expected", [
    ( # Case: valid list
        [
        actions.Statement("test1"),
        actions.Decision("test2"),
        actions.Loop("test3")
        ], 
        True
    ),
    ( # Case: valid list with empty scenes
        [
        actions.Statement("test1"),
        None,
        actions.Decision("test2"),
        actions.Loop("test3"),
        None,
        ], 
        True
    ),
    (# Case: more than 5 empty scenes  
        [
        None, None, None, None, None, None,
        actions.Statement("test1"),
        actions.Decision("test2"),
        actions.Loop("test3")
        ], 
        False
    ), 
    ( # Case: missing statement
        [
        actions.Decision("test1"),
        actions.Loop("test2")
        ], 
        False
    ),
    ( # Case: missing decision
        [
        actions.Statement("test1"),
        actions.Loop("test2")
        ], 
        False
    ),
    ( # Case: missing loop
        [
        actions.Statement("test1"),
        actions.Decision("test2")
        ], 
        False
    ),
    ( # Case: two statements in a row
        [
        actions.Statement("test1"),
        actions.Statement("test2"),
        actions.Decision("test3"),
        actions.Loop("test4")
        ], 
        False
    ),
    ( # Case: all types but exceeding 5 empty scenes:
        [
        actions.Statement("test1"),
        actions.Decision("test2"),
        None, None, None, None, None, None,
        actions.Loop("test3")
        ], 
        False
    ),
    ( # Case: all empty scenes
        [
        None, None, None, None, None
        ], 
        False
    ),
    ( # Case: empty list
        [], 
        False
    )
])
def test_gameRunner_listValid(sceneList, expected):
    # Setup
    gr = gameRunner.GameRunner()
    gr._GameRunner__sceneList = sceneList

    # Run
    result = gr._GameRunner__listValid()

    # Verify
    assert result == expected


#-------------------------------------------------------
# __setup
#-------------------------------------------------------
def test_gameRunner_setup(mock_gameRunner_pickRandScene, mock_gameRunner_getName, mock_gameRunner_listValid):
    # Setup
    gr = gameRunner.GameRunner()

    # Run
    gr.setup()

    # Verify that getName was called
    assert mock_gameRunner_getName.called == True

    # Verify that pickRandScene was called 10 times
    assert mock_gameRunner_pickRandScene.call_count == 10

    # Verify that sceneList has a length of 10
    assert len(gr._GameRunner__sceneList) == 10

    # Verify that setup was marked as complete
    assert gr._GameRunner__isSetup == True


#-------------------------------------------------------
# run
#-------------------------------------------------------
def test_gameRunner_run(mock_decision_performAction, mock_input_str):
    # Setup
    gr = gameRunner.GameRunner()
    gr._GameRunner__isSetup = True
    gr._GameRunner__sceneList = [
        actions.Decision("test1"),
        actions.Decision("test2"),
        actions.Decision("test3"),
        actions.Decision("test4"),
        actions.Decision("test5"),
        actions.Decision("test6"),
        actions.Decision("test7"),
        actions.Decision("test8"),
        actions.Decision("test9"),
        actions.Decision("test10")
        ]

    # Run
    gr.run()

    car = len(gr._GameRunner__sceneList)

    # Verify
    assert gr._GameRunner__bst.getSize() == len(gr._GameRunner__sceneList)


#-------------------------------------------------------
# __getFiller
#-------------------------------------------------------
@pytest.mark.parametrize("fillerFiller, expected", [
    (["This is a [NAME] test, 1 2 3"], "This is a testName test, 1 2 3"), # Case: filler with 1 [NAME]
    (["This is a [NAME] test, 1 2 3 [NAME]"], "This is a testName test, 1 2 3 testName"), # Case: filler with 2 [NAME]
    (["This is a test, 1 2 3"], "This is a test, 1 2 3") # Case: filler with no [NAME] 
])
def test_gameRunner_getFiller(fillerFiller, expected):
    # Setup
    gr = gameRunner.GameRunner()
    gr._GameRunner__filler = fillerFiller
    gr._GameRunner__playerName = "testName"

    # Run
    result = gr._GameRunner__getFiller()

    # Verify
    assert result == expected