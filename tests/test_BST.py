'''
 * Name: Demetri Van Sickle
 * Date: 06/02/2024
 * Class: CS302
 * Assignment: Program 4/5
 * Description:
 *      Test suite for BST.py
'''
import pytest
import BST

#-------------------------------------------------------
# constructor
#-------------------------------------------------------
def test_bst_constructor():
    bst = BST.BST()
    assert bst._BST__root == None
    assert bst._BST__size == 0

#-------------------------------------------------------
# insert
#-------------------------------------------------------
@pytest.mark.parametrize("items, expectedSize", [
    ([10,5,7,20,1,40], 6), # No dups
    ([1,10,5,7,5,20,1,40,40], 9) # With dups
])
def test_bst_insert(items, expectedSize):
    bst = BST.BST()

    # Create tree
    for item in items:
        bst.insert(item)

    # Traverse the tree in order and get the list
    result = bst._BST__inorderTraversal()

    expected = sorted(items)

    assert result == expected
    assert bst._BST__size == expectedSize


#-------------------------------------------------------
# retrieve
#-------------------------------------------------------
@pytest.mark.parametrize("items, toFind, expected", [
    ([10,5,7,20,1,40], 50, None),
    ([10,5,7,20,1,40], 5, 5),
    ([10,5,7,20,1,40], 40, 40),
    ([1,10,5,7,5,20,1,40,40], 5, 5),
    ([1,10,5,7,5,20,1,40,40], 50, None),
    ([10,5,7,1,5,20,1,40,40], 7, 7),
])
def test_bst_retrieve(items, toFind, expected):
    bst = BST.BST()

    # Create tree
    for item in items:
        bst.insert(item)

    # Get the item
    result = bst.retrieve(toFind)

    if expected == None:
        assert result == None
    else:
        assert result.getData() == expected

    # Make sure tree is still intact
    result = bst._BST__inorderTraversal()
    expected = sorted(items)
    assert result == expected


#-------------------------------------------------------
# remove
#-------------------------------------------------------
@pytest.mark.parametrize("items, toRemove, finalSize", [
    ([10, 5, 15, 3, 7, 12, 18, 5, 19], 3, 8), # Case: leaf
    ([10, 5, 15, 3, 7, 12, 18, 5, 19], 15, 8), # Case: node w/ both children and IOS is parent's right
    ([10, 5, 15, 3, 7, 12, 18, 5, 19, 16], 15, 9), # Case: node w/ both children and IOS is w/o children
    ([10, 5, 15, 3, 7, 12, 18, 5, 19, 16, 17], 15, 10), # Case: node w/ both children and IOS w/ right child
    ([10, 5, 15, 3, 7, 12, 18, 5, 19], 18, 8), # Case: node w/ only right child
    ([10, 5, 15, 3, 7, 12, 18, 5, 19], 7, 8), # Case: node w/ only left child
    ([10, 5, 15, 3, 7, 12, 18, 5, 19], 5, 7), # Case: dup nodes
    ([10, 5, 15, 3, 7, 12, 18, 5, 19], 10, 8), # Case: remove root
    ([10, 5, 15, 3, 7, 12, 18, 5, 19], 20, 9), # Case: item not in tree
])
def test_bst_remove(items, toRemove, finalSize):
    expected = []

    bst = BST.BST()

    # Create tree
    for item in items:
        bst.insert(item)

    # Remove the item
    bst.remove(toRemove)

    # Calculate our expected list
    for item in items:
        if item != toRemove:
            expected.append(item)
    expected = sorted(expected)

    # Make sure tree is still intact
    result = bst._BST__inorderTraversal()
    assert result == expected
    
    # Make sure size is correct
    assert bst._BST__size == finalSize


#-------------------------------------------------------
# gotTotal
#-------------------------------------------------------
@pytest.mark.parametrize("items, expected", [
    ([5,0,-7,3,-1,-4], -4),
    ([-3, 2, -1, 4, -2], 0),
    ([1, 3, 4, 2, 2], 12),
    ([-4, -2, -3, -1, -4], -14),
    ([0, 0, 0, 0, 0], 0)
])
def test_bst_retrieve(items, expected):
    bst = BST.BST()

    # Create tree
    for item in items:
        bst.insert(item)

    # Get the item
    result = bst.getTotal()

    assert result == expected
    