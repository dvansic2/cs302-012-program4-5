'''
 * Name: Demetri Van Sickle
 * Date: 05/31/2024
 * Class: CS302
 * Assignment: Program 4/5
 * Description:
 *      Test suite for actions.py
'''
import pytest
import numpy as np
import actions

#--------------------------------------------------------------------------------
# BEGIN Actions class tests
#--------------------------------------------------------------------------------
def test_action_valid_name():
    # Create object
    action = actions.Actions("test")
    
    # Make sure name is set correctly
    assert action._name == "test"

    # Make sure other values are set correctly
    assert action._score == 0
    assert action._correctIndex == 0
    assert action._failMessages == None
    assert action._successMessages == None
    assert action._descriptions == None
    assert action._userInput == None

def test_action_invalid_name():
    # Test non string
    with pytest.raises(ValueError):
        action = actions.Actions(123)

    # test empty string
    with pytest.raises(ValueError):
        action = actions.Actions("")

def test_action_lt():
    obj1 = actions.Actions("test")
    obj1._score = 10

    obj2 = actions.Loop("test")
    obj2._score = 5

    assert (obj2 < obj1) == True
    assert (obj1 < obj2) == False
    assert (obj1 < 40) == True
    assert (3 < obj1) == True


def test_action_ge():
    obj1 = actions.Actions("test")
    obj1._score = 10

    obj2 = actions.Loop("test")
    obj2._score = 5

    obj3 = actions.Statement("test")
    obj3._score = 10

    assert (obj2 >= obj1) == False
    assert (obj1 >= obj2) == True
    assert (obj1 >= obj3) == True
    assert (20 >= obj3) == True
    assert (obj3 >= 5) == True

def test_action_eq():
    obj1 = actions.Actions("test")
    obj1._score = 10

    obj2 = actions.Loop("test")
    obj2._score = 5

    obj3 = actions.Statement("test")
    obj3._score = 10

    assert (obj2 == obj1) == False
    assert (obj1 == obj3) == True
    assert (obj1 == 10) == True
    assert (10 == obj1) == True

#--------------------------------------------------------------------------------
# END Actions class tests
#--------------------------------------------------------------------------------



#--------------------------------------------------------------------------------
# BEGIN Statement class tests
#--------------------------------------------------------------------------------
def test_statement_constructor(statementObj):
    # Make sure commands are set correctly
    assert np.array_equal(statementObj._Statement__commands, np.array([
        "move left",
        "move right",
        "speed up",
        "slow down",
    ]))

    # Make sure success messages are set correctly
    assert len(statementObj._successMessages) == 4

    # Make sure fail messages are set correctly
    assert len(statementObj._failMessages) == 4

    # Make sure descriptions are set correctly
    assert len(statementObj._descriptions) == 4

    # Make sure correct index is set correctly
    assert statementObj._correctIndex >= 0 and statementObj._correctIndex < 4


#-------------------------------------------------------
# performAction CORRECT
#   w/ mocked input()
#-------------------------------------------------------
@pytest.mark.parametrize("mock_input, correctIndex", [
    ("move left", 0),
    ("move right", 1),
    ("speed up", 2),
    ("slow down", 3),
], indirect=["mock_input"])
def test_statment_performAction_correct(mock_input, correctIndex, statementObj):
    # Set the correct index
    statementObj._correctIndex = correctIndex
    
    # Run the function
    statementObj.performAction()

    # Make sure score is within the correct range
    assert statementObj._score >= 1 and statementObj._score <= 4


#-------------------------------------------------------
# performAction NOT CORRECT
#   w/ mocked input()
#-------------------------------------------------------
@pytest.mark.parametrize("mock_input, correctIndex", [
    ("move left", 3),
    ("move right", 2),
    ("speed up", 1),
    ("slow down", 0),
], indirect=["mock_input"])
def test_statment_performAction_incorrect(mock_input, correctIndex, statementObj):
    # Set the correct index
    statementObj._correctIndex = correctIndex
    
    # Run the function
    statementObj.performAction()

    # Make sure score is within the correct range
    assert statementObj._score <= -1 and statementObj._score >= -4

#-------------------------------------------------------
# performAction INVALID
#   w/ mocked input()
#-------------------------------------------------------
@pytest.mark.parametrize("mock_input, correctIndex", [
    ("", 0),
    ("", 1),
    ("", 2),
    ("", 3),
    (123, 0),
    (123, 1),
    (123, 2),
    (123, 3),
], indirect=["mock_input"])
def test_statment_performAction_invalid(mock_input, correctIndex, statementObj):
    # Set the correct index
    statementObj._correctIndex = correctIndex
    
    # Run the function
    with pytest.raises(ValueError):
        statementObj.performAction()

#-------------------------------------------------------
# __validateCommand VALID
#-------------------------------------------------------
@pytest.mark.parametrize("userInput", [
    ("move left"),
    ("move right"),
    ("speed up"),
    ("slow down"),
    ("Move left"),
    ("move rIght"),
    ("spEEd up"),
    ("sLow dOwn"),
])
def test_statement_validateCommand_valid(userInput, statementObj):
    # Set userInput
    statementObj._userInput = userInput

    # Test statement
    assert statementObj._Statement__validateCommand() == True


#-------------------------------------------------------
# __validateCommand INVALID
#-------------------------------------------------------
@pytest.mark.parametrize("userInput", [
    ("left"),
    ("mov right"),
    ("speed up "),
    ("slow"),
    ("sddfdf"),
    (""),
    (123),
])
def test_statement_validateCommand_invalid(userInput, statementObj):
    # Set userInput
    statementObj._userInput = userInput

    # Test statement
    assert statementObj._Statement__validateCommand() == False


#-------------------------------------------------------
# __executeCommand CORRECT
#-------------------------------------------------------
@pytest.mark.parametrize("userInput, correctIndex", [
    ("move left", 0),
    ("move right", 1),
    ("speed up", 2),
    ("slow down", 3),
])
def test_statement_executeCommand_correct(userInput, correctIndex, statementObj, capsys):
    # Set userInput
    statementObj._userInput = userInput

    # Set the correct index
    statementObj._correctIndex = correctIndex

    # Test statement
    assert statementObj._Statement__executeCommand() == True

    # Capture output and validate that somthing was printed
    captured = capsys.readouterr()
    assert len(captured.out) > 0


#-------------------------------------------------------
# __executeCommand NOT CORRECT
#-------------------------------------------------------
@pytest.mark.parametrize("userInput, correctIndex", [
    ("move left", 3),
    ("move right", 2),
    ("speed up", 1),
    ("slow down", 0),
])
def test_statement_executeCommand_incorrect(userInput, correctIndex, statementObj, capsys):
    # Set userInput
    statementObj._userInput = userInput

    # Set the correct index
    statementObj._correctIndex = correctIndex

    # Test statement
    assert statementObj._Statement__executeCommand() == False

    # Capture output and validate that somthing was printed
    captured = capsys.readouterr()
    assert len(captured.out) > 0

#--------------------------------------------------------------------------------
# END Statement class tests
#--------------------------------------------------------------------------------



#--------------------------------------------------------------------------------
# BEGIN Decision class tests
#--------------------------------------------------------------------------------
def test_decision_constructor(decisionObj):
    # Make sure actions are set correctly
    assert len(decisionObj._Decision__actions) == 2

    # Make sure evaluations are set correctly
    assert len(decisionObj._Decision__evaluations) == 2

    # Make sure success messages are set correctly
    assert len(decisionObj._successMessages) == 2

    # Make sure fail messages are set correctly
    assert len(decisionObj._failMessages) == 2

    # Make sure descriptions are set correctly
    assert len(decisionObj._descriptions) == 2

    # Make sure correct index is set correctly
    assert decisionObj._correctIndex >= 0 and decisionObj._correctIndex < 2

    # Make sure parsedUserMessage is initialized 
    assert decisionObj._Decision__parsedUserStatement == None


#-------------------------------------------------------
# __validateAndParse VALID
#-------------------------------------------------------
@pytest.mark.parametrize("userInput, expected", [
    ("if [ship is hostile] then [fire at ship]", ["ship is hostile", "fire at ship"]),
    ("if [ship is hostile] then [hail ship]", ["ship is hostile", "hail ship"]),
    ("if [ship is friendly] then [fire at ship]", ["ship is friendly", "fire at ship"]),
    ("if [ship is friendly] then [hail ship]", ["ship is friendly", "hail ship"]),
    ("if [Ship is Friendly] thEn [hail sHiP]", ["ship is friendly", "hail ship"]),
])
def test_decision_validateAndParse_valid(userInput, expected, decisionObj):
    # Set the user input
    decisionObj._userInput = userInput

    # Call the function
    assert decisionObj._Decision__validateAndParse() == True

    # Make sure input was parsed 
    assert decisionObj._Decision__parsedUserStatement == expected


#-------------------------------------------------------
# __validateAndParse VALID parse, INVALID contents
#-------------------------------------------------------
@pytest.mark.parametrize("userInput", [
    ("if [not valid] then [fire at ship]"),
    ("if [ship is hostile] then [not valid]"),
    ("if [not valid] then [not valid]"),
])
def test_decision_validateAndParse_validP_invalidC(userInput, decisionObj):
    # Set the user input
    decisionObj._userInput = userInput

    # Make sure call fails
    assert decisionObj._Decision__validateAndParse() == False

    # Make sure input was parsed
    assert len(decisionObj._Decision__parsedUserStatement) == 2


#-------------------------------------------------------
# __validateAndParse INVALID parse
#-------------------------------------------------------
@pytest.mark.parametrize("userInput", [
    ("if ship is hostile then fire at ship"),
    ("if [ship is hostile] then fire at ship"),
    ("if [ship is hostile] then [fire at ship"),
    ("if [ship is hostile] then fire at ship]"),
    (""),
    (123),
])
def test_decision_validateAndParse_invalid(userInput, decisionObj):
    # Set the user input
    decisionObj._userInput = userInput

    # Make sure call fails
    assert decisionObj._Decision__validateAndParse() == False


#-------------------------------------------------------
# __testStatement CORRECT
#-------------------------------------------------------
@pytest.mark.parametrize("parsedStatements, correctIndex", [
    (["ship is hostile", "fire at ship"], 0),
    (["ship is friendly", "hail ship"], 1),
])
def test_decision_testStatement_correct(parsedStatements, correctIndex, decisionObj, capsys):
    # Set the parsed statement
    decisionObj._Decision__parsedUserStatement = parsedStatements

    # Set the correct index
    decisionObj._correctIndex = correctIndex

    # Call should return true
    assert decisionObj._Decision__testStatement() == True

    # Capture output and validate that somthing was printed
    captured = capsys.readouterr()
    assert len(captured.out) > 0


#-------------------------------------------------------
# __testStatement NOT CORRECT
#-------------------------------------------------------
@pytest.mark.parametrize("parsedStatements, correctIndex", [
    (["ship is hostile", "hail ship"], 0),
    (["ship is friendly", "fire at ship"], 0),
    (["ship is friendly", "hail ship"], 0),

    (["ship is hostile", "fire at ship"], 1),
    (["ship is hostile", "hail ship"], 1),
    (["ship is friendly", "fire at ship"], 1),
])
def test_decision_testStatement_incorrect(parsedStatements, correctIndex, decisionObj, capsys):
    # Set the parsed statement
    decisionObj._Decision__parsedUserStatement = parsedStatements

    # Set the correct index
    decisionObj._correctIndex = correctIndex

    # Call should return false
    assert decisionObj._Decision__testStatement() == False

    # Capture output and validate that somthing was printed
    captured = capsys.readouterr()
    assert len(captured.out) > 0


#-------------------------------------------------------
# performAction
#   w/mocked functions
#-------------------------------------------------------
@pytest.mark.parametrize("mock_decision_validateAndParse, mock_decision_testStatement, expectException", [
    (True, True, False), # valid input and parse AND correct choice
    (True, False, False), # valid input and parse AND incorrect choice
    (False, None, True), # invalid input
], indirect=["mock_decision_validateAndParse", "mock_decision_testStatement"])
def test_decision_performAction(mock_decision_validateAndParse, mock_decision_testStatement, expectException, capsys, mock_input_str, decisionObj):
    if expectException:
        with pytest.raises(ValueError):
            decisionObj.performAction()
    else:
        decisionObj.performAction()

        if mock_decision_testStatement():
            assert decisionObj._score >= 1 and decisionObj._score <= 4
        else:
            assert decisionObj._score <= -1 and decisionObj._score >= -4

        # Capture output and validate that somthing was printed
        captured = capsys.readouterr()
        assert len(captured.out) > 0

#--------------------------------------------------------------------------------
# END Decision class tests
#--------------------------------------------------------------------------------




#--------------------------------------------------------------------------------
# BEGIN Loop class tests
#--------------------------------------------------------------------------------
def test_loop_constructor(loopObj):
    # Make sure success messages are set correctly
    assert len(loopObj._successMessages) == 1

    # Make sure fail messages are set correctly
    assert len(loopObj._failMessages) == 1

    # Make sure descriptions are set correctly
    assert len(loopObj._descriptions) == 1

    # Make sure correct index is set correctly
    assert loopObj._correctIndex >= 0 and loopObj._correctIndex < 1

    # Make sure local member variable are set correctly
    assert loopObj._Loop__numAsteroidsCollected == 0
    assert loopObj._Loop__numAsteroids >= 4 and loopObj._Loop__numAsteroids <= 10


#-------------------------------------------------------
# __collectSample
#   w/ mocked input
#-------------------------------------------------------
@pytest.mark.parametrize("mock_input", [
    ("0"),
    ("1")
], indirect=["mock_input"])
def test_loop_collectSample(mock_input, loopObj):
    # Get the current value of numAsteroidsCollected
    currVal = loopObj._Loop__numAsteroidsCollected

    # Call the function
    loopObj._Loop__collectSample()

    if mock_input() == "0":
        assert currVal > loopObj._Loop__numAsteroidsCollected
    else:
        assert currVal < loopObj._Loop__numAsteroidsCollected


#-------------------------------------------------------
# __isComplete
#-------------------------------------------------------
@pytest.mark.parametrize("numCollected, numToCollect, expected", [
    (7, 5, True), # numCollected > numToCollect
    (-1, 5, True), # numCollected < 0
    (2, 5, False) # Still some to collect
])
def test_loop_isComplete(numCollected, numToCollect, expected, loopObj):
    # Set the variables
    loopObj._Loop__numAsteroidsCollected = numCollected
    loopObj._Loop__numAsteroids = numToCollect

    # Call function and check return
    assert loopObj._Loop__isComplete() == expected


#-------------------------------------------------------
# __getResult
#-------------------------------------------------------
@pytest.mark.parametrize("numCollected, numToCollect, expected", [
    (7, 5, True), # numCollected > numToCollect
    (-1, 5, False), # numCollected < 0
])
def test_loop_getResult(numCollected, numToCollect, expected, loopObj, capsys):
    # Set the variables
    loopObj._Loop__numAsteroidsCollected = numCollected
    loopObj._Loop__numAsteroids = numToCollect

    # Call function and check return
    assert loopObj._Loop__getResult() == expected

    # Capture output and validate that somthing was printed
    captured = capsys.readouterr()
    assert len(captured.out) > 0


#-------------------------------------------------------
# performAction
#   w/ mocked input
#-------------------------------------------------------
@pytest.mark.parametrize("mock_input, expected", [
    ("0", False), # will cause the num collected to go negative
    ("1", True)  # will caust the num collected to meet the goal
], indirect=["mock_input"])
def test_statment_performAction_correct(mock_input, expected, loopObj, capsys):
    # Call the function 
    loopObj.performAction()

    # Capture output and validate that somthing was printed
    captured = capsys.readouterr()
    assert len(captured.out) > 0

    # Verify the result is as expected
    assert loopObj._Loop__getResult() == expected

    # Verify that the score was set correctly 
    if expected:
        assert loopObj._score >= 1 and loopObj._score <= 4
    else:
        assert loopObj._score <= -1 and loopObj._score >= -4

#-------------------------------------------------------
# END Loop class tests
#-------------------------------------------------------